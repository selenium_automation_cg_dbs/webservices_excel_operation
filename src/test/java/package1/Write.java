package package1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Write {

	public static void writeData(String str) throws IOException {

		FileInputStream inp = new FileInputStream("D:\\Excel_Driven\\src\\data.xlsx");
		XSSFWorkbook wb = new XSSFWorkbook(inp);
		Sheet sheet = wb.getSheetAt(1);
		Row row = sheet.createRow(1);
		row.createCell(0).setCellValue(str);

		FileOutputStream fileOut = new FileOutputStream("D:\\Excel_Driven\\src\\data.xlsx");
		wb.write(fileOut);
		fileOut.close();

	}

}
