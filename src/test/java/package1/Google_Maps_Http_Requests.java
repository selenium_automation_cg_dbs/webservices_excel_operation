package package1;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;

public class Google_Maps_Http_Requests {

	public static void main(String[] args) throws IOException {

		Read_Write r = new Read_Write();
		ArrayList data = r.getData("Book1");

		System.out.println(data.get(1));
		System.out.println(data.get(2));
		System.out.println(data.get(3));
		System.out.println(data.get(4));

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("name", data.get(1));
		map.put("isbn", data.get(2));
		map.put("aisle", data.get(3));
		map.put("author", data.get(4));

		RestAssured.baseURI = "http://216.10.245.166";
		// add book
		Response response = given().log().all().header("Content-Type", "application/json").body(map).when()
				.post("Library/Addbook.php").then().assertThat().statusCode(200).extract().response();

		System.out.println(response);
		JsonPath js = ReusableMethods.rawToJson(response);
		String id = js.getString("ID");
		System.out.println("Book id is " + id);

		Write.writeData(id);

		// Get Book id

	
		  Response getIdResponse = given().log().all().queryParam("ID",
		  id).when().get("Library/GetBook.php").then()
		  .assertThat().log().all().statusCode(200).extract().response();
		  
		  JsonPath js1 = ReusableMethods.rawToJson(getIdResponse);
		  String bookname = js1.getString("book_name").toString();
		
		  
		 System.out.println("fetching book name " +  bookname);
		 
		 

	}

}
